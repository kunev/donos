#DONOS(Донос)

    ДОНОС
    1. denunciation, delation, report to the authorities (срещу against);

    2. (клеветничество) slander, calumny;

    правя ДОНОС вж. доноснича.

Donos is an *extremely stupid* set of tiny tools to exchange status information between devices. The only reason I've taken up writing this is because I want a way to view my Jolla's stats from my laptop.

This repo currently holds a few little things:

###donosd
A daemon that uploads a json with information to a specified server($SNITCHSERVER) via rysnc as a specified user($SNITCHUSER).

###get-donos
Downloads the donos file from the server

## Disclaimer
I've written this for personal use and still have to implement the client part that will run on the machine interested in reading the information being sent to the server.

### Security?
This basically sends a json file with your current SSID, local IP address and battery percentage to a server of your choice via rsync. If you think this is sensitive information than sending it to a server you do not trust is *stupid*, doing this via a network you assume to be insecure is also *stupid*.


##Possible TODOs

 * Some type of real client that does something with the information it gets from the server
 * Possibly a tool to extract exact information from the json file
 * "Blueprints" on integrating the information into your DE/WM/status bar/whatever floats your boat
 * …
 * ~~Plot to monetize through inserting NSA backdoor in the code.~~
